
help:
	@grep -E ': .*[^]*]## ' Makefile | sed 's/:.*## /\t/'

log-work: work-log.md  ## Put unified entry in work-log.md.  Requires $(LW)
	@if [[ ! -z "$(LW)" ]] ; then echo - '`'$$(date)'`' -- $(LW) >> $<; else echo Say something'!'; fi
	@tail -10 $<

lw: log-work

.PHONY: lw
