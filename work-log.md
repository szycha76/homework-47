# Initial work
- `Sat May 28 15:39:10 CEST 2022` --  Created empty repository on local disk
- `Sat May 28 15:42:19 CEST 2022` --  Created new "project" on gitlab.com
- `Sat May 28 15:47:10 CEST 2022` --  Created a Makefile
- `Sat May 28 15:50:07 CEST 2022` --  Pushed to GitLab

# Task 1: Create an API service
- `Sat May 28 15:51:43 CEST 2022` --  Looking in Google for "tiniest RESTful API"
- `Sat May 28 15:54:39 CEST 2022` --  [https://github.com/teelinsan/minimal-rest-microservice][mini-rest] looks promising
- `Sat May 28 15:55:40 CEST 2022` --  (cloned)
- `Sat May 28 15:57:38 CEST 2022` --  Created orphan branch "[rest][rest]"
- `Sat May 28 16:28:39 CEST 2022` --  Now I have two repositories in one, in separate branches
- `Sat May 28 16:30:45 CEST 2022` --  **TODO**: learn how to write proper REST microservices myself, for now will use work from [Andrea][andrea].

[andrea]: https://github.com/teelinsan
[mini-rest]: https://github.com/teelinsan/minimal-rest-microservice
[rest]: ../../tree/rest
[d]: ../../tree/rest/account-microservice/Dockerfile
[di]: ../../tree/rest/account-microservice/.dockerignore
[dc]: ../../tree/rest/docker-compose.yml
[h]: https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz

30 minutes spent so far with ~20 minutes of interruptions till this point, 3h30m left
- `Sat May 28 23:32:59 CEST 2022` --  Adding URLs to this log
- `Sat May 28 23:45:29 CEST 2022` --  Started the application, added one user ("szycha"). It is fun to see it working!

# Task 2: Dockerize
Well, [`Dockerfile`][d] was already in place, hence I skipped this part and focused on optimization.
Changes:
- used python:3.9-alpine instead of python:3.6 base image to save 95% of space in base image
- used [`.dockerignore`][di] ~and layered builds to further optimize the image~ (**TODO**)
- there's no official `MongoDB` package for `Alpine Linux` as of now, so no improvement of this sort here, unfortunately.

# Task 3: Create a helm chart

I'm using [Helm v3.9.0 for `amd64`][h].
```shell
helm create homework-47
```

